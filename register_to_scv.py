import pandas


class Register:
    all_info: dict

    def __init__(self):
        df = None

    @staticmethod
    def register_info(all_info):
        try:
            df = pandas.DataFrame(all_info)
            df.to_csv('all_weather_info.csv', index=False)
        except Exception as e:
            print(f'Error trying to register info in csv file: {e}')
