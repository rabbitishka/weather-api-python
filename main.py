import sys
sys.path.append('..')
from cities_reader import Cities
from send_request import SendRequest
from register_to_scv import Register


cities = Cities.get_cities()
request_weather = SendRequest()

all_data_list = []

for city in cities:
    request_weather.location = city
    weather_data = request_weather.get_weather()

    for day in weather_data['days']:
        datetime = day['datetime']
        temp_f = day['temp']
        temp_c = SendRequest.far_to_celsius(farenheit_temp=temp_f)
        all_data_list.append({'City': city, 'Date': datetime, 'Temperature (Celsius)': temp_c})

Register.register_info(all_data_list)


