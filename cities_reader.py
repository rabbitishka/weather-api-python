import yaml


class Cities:

    @staticmethod
    def get_cities():
        try:
            with open('yaml_config/cities.yaml') as file:
                cities_data = yaml.load(file, Loader=yaml.FullLoader)
                cities = cities_data.get('cities', [])
                return cities
        except Exception as e:
            print(f'Error getting cities from yaml file: {e}')


#cities_from_yaml = Cities.get_cities()
#print(cities_from_yaml)