import requests
from datetime import datetime
import sys
sys.path.append('..')
from config.global_config import *


class SendRequest:
    location: str

    def __init__(self):
        try:
            self.API_KEY = API_KEY
            self.URL = URL
            self.location = ''
            self.start_date = START_DATE
            self.end_date = END_DATE
        except Exception as e:
            print(f'Error initializing SendRequest: {e}')

# [location]/[date1]/[date2]?key=YOUR_API_KEY'

    @staticmethod
    def far_to_celsius(farenheit_temp: float):
        try:
            return round((farenheit_temp - 32) * 5 / 9)
        except Exception as e:
            print(f'Error changing Farenheit in Celsius: {e}')

    def get_weather(self):
        try:
            url_keys = f"{self.URL}{self.location}/{self.start_date}/{self.end_date}?key={self.API_KEY}"
            r = requests.get(url=url_keys)
            result = r.json()
            #print(r.text)
            return result
        except Exception as e:
            print(f'Error getting the weather data: {e}')


#request = SendRequest()
#url = request.get_weather()
#print(url)